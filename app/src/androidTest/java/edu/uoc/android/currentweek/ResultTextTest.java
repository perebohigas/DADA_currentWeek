package edu.uoc.android.currentweek;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ResultTextTest {

    private static Integer currentWeekValue;
    private static Calendar calendar;

    @Rule
    public ActivityTestRule<MainActivity> ActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp(){
        calendar = Calendar.getInstance();
        currentWeekValue = calendar.get(Calendar.WEEK_OF_YEAR);
    }

    @Test
    public void checkRightMessage() {
        // Type value into the input field
        onView(withId(R.id.currentWeekInput))
                .perform(typeText(String.valueOf(currentWeekValue)), closeSoftKeyboard());

        // Press the button
        onView(withId(R.id.buttonCheck))
                .perform(click());

        // Check the result's message
        onView(withId(R.id.textResult))
                .check(matches(withText(R.string.results_right)));
    }

    @Test
    public void checkWrongMessage() {
        // Type value into input field
        onView(withId(R.id.currentWeekInput)).perform(typeText(String.valueOf(currentWeekValue-1)), closeSoftKeyboard());

        // Press the button
        onView(withId(R.id.buttonCheck)).perform(click());

        // Check the result's message
        onView(withId(R.id.textResult)).
                check(matches(withText(R.string.results_wrong)));
    }

}
