package edu.uoc.android.currentweek;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button buttonCheck;
    private EditText currentWeekInput;

    public static final String EXTRA_CURRENTWEEKVALUE = "edu.uoc.android.currentweek.CurrentWeekValue";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setViews();
    }

    /** Method to set views from activity_main.xml to this class **/
    private void setViews() {
        /** Link button with the defined variable **/
        buttonCheck = findViewById(R.id.buttonCheck);
        /** Set a listener for a click event on buttonCheck **/
        buttonCheck.setOnClickListener(this);
        /** Link input box with the defined variable **/
        currentWeekInput = findViewById(R.id.currentWeekInput);
    }

    /** Method to check if the user has introduced a valid value **/
    private boolean hasValidValue() {
        /** It will only check if the edit text is not empty (cause the other possible malfunctioning cases has been already taken in consideration) **/
        return !currentWeekInput.getText().toString().isEmpty();
    }

    private int getWeekNumberValue() {
        /** Get the value from the input text field and parse it into an integer **/
        return Integer.parseInt(currentWeekInput.getText().toString());
    }

    private void goToResultsActivity() {
        /** Create an intent to go to ResultsActivity **/
        Intent intent = new Intent(this, ResultActivity.class);
        /** Get value from input field and add it to the intent **/
        intent.putExtra(EXTRA_CURRENTWEEKVALUE, this.getWeekNumberValue());
        /** Start the ResultsActivity **/
        startActivity(intent);
    }

    @Override
    /** Method called when the user touches de button **/
    public void onClick(View view) {
        /** Proceed if the value is correct or not **/
        if (this.hasValidValue()) {
            /** Go to ResultsActivity **/
            this.goToResultsActivity();
        } else {
            /** Show an error message **/
            currentWeekInput.setError(getString(R.string.main_error));
        }
    }

}
